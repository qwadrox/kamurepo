﻿// <copyright file="LoadGameWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Urlenyinvazio
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;
    using Newtonsoft.Json.Linq;
    using Urlenyinvazio.Logic.Interfaces;
    using Urlenyinvazio.Logic.LogicClasses;
    using Urlenyinvazio.Model.ModelClasses;

    /// <summary>
    /// Interaction logic for LoadGameWindow.xaml.
    /// </summary>
    public partial class LoadGameWindow : Window
    {
        private readonly IRepositoryLogic repositoryLogic;
        public LoadGameWindow()
        {
            this.InitializeComponent();
            this.repositoryLogic = new RepositoryLogic();

            List<SavedGame> games = this.LoadGames();
            this.LoadGameList.ItemsSource = games;
            if (this.LoadGameList.SelectedItem != null)
            {
                SavedGame selectedGame = (SavedGame)this.LoadGameList.SelectedItem;
                this.Content = new Controller.GameController(this.repositoryLogic.LoadGameModel(selectedGame.Name));
            }
        }

        private void LoadGameButtonClick(object sender, RoutedEventArgs e)
        {
            if (this.LoadGameList.SelectedItem != null)
            {
                SavedGame selectedGame = (SavedGame)this.LoadGameList.SelectedItem;
                MainWindow mainwindow = new MainWindow(this.repositoryLogic.LoadGameModel(selectedGame.Name));
                mainwindow.Show();
                this.Close();
            }
        }

        private List<SavedGame> LoadGames()
        {
            List<SavedGame> games = new List<SavedGame>();
            string path = System.IO.Directory.GetCurrentDirectory();
            string[] foundFilePaths = Directory.GetFiles(Directory.GetCurrentDirectory(), "*." + "json");
            foreach (string filePath in foundFilePaths)
            {
                string fileName = filePath.Split('\\')[filePath.Split('\\').Length - 1].Split(new string[] { ".json" }, StringSplitOptions.None)[0];
                if (fileName != "highScores")
                {
                    using (StreamReader r = new StreamReader(filePath))
                    {
                        string json = r.ReadToEnd();
                        CultureInfo enUS = new CultureInfo("en-US");
                        JObject jsonObject = JObject.Parse(json);

                        SavedGame game = new SavedGame
                        {
                            Name = fileName,
                            Date = DateTime.Parse((string)jsonObject["SaveDate"]),
                            Score = (int)jsonObject["HighScore"],
                            Time = (int)jsonObject["Time"],
                        };
                        games.Add(game);
                    }
                }
            }

            return games;
        }

        private void BackToMenuButtonClick(object sender, RoutedEventArgs e)
        {
            MenuWindow menuWindow = new MenuWindow();
            menuWindow.Show();
            this.Close();
        }
    }
}
