﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Urlenyinvazio.Model.ModelClasses;
using Urlenyinvazio.Logic.LogicClasses;
using Urlenyinvazio.Logic.Interfaces;
using Urlenyinvazio.Model;

namespace Urlenyinvazio
{
    /// <summary>
    /// Interaction logic for HighScoreWindow.xaml.
    /// </summary>
    public partial class HighScoreWindow : Window
    {
        private readonly IRepositoryLogic repositoryLogic;
        public HighScoreWindow()
        {
            this.InitializeComponent();
            this.repositoryLogic = new RepositoryLogic();
            this.ScoreList.ItemsSource = this.LoadhighScores();
        }

        private List<SavedGame> LoadhighScores()
        {
            List<SavedGame> highScores = this.repositoryLogic.LoadHighScores();
            return highScores.OrderBy(x => x.Score).Reverse().ToList();
        }

        private void BackToMenuButtonClick(object sender, RoutedEventArgs e)
        {
            MenuWindow menuWindow = new MenuWindow();
            menuWindow.Show();
            this.Close();
        }
    }
}
