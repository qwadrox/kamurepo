﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("", "CA1014", Justification = "<NikGitStats>", Scope = "module")]
[assembly: SuppressMessage("", "CA1056", Justification = "<NikGitStats>", Scope = "module")]
[assembly: SuppressMessage("", "SA1404", Justification = "<NikGitStats>", Scope = "module")]
[assembly: SuppressMessage("", "CA2000", Justification = "<NikGitStats>", Scope = "module")]