﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Urlenyinvazio
{
    /// <summary>
    /// Interaction logic for MenuWindow.xaml.
    /// </summary>
    public partial class MenuWindow : Window
    {
        public MenuWindow()
        {
            this.InitializeComponent();
        }

        private void RunMainWindowClick(object sender, RoutedEventArgs e)
        {
            MainWindow mainwindow = new MainWindow();
            mainwindow.Show();
            this.Close();
        }

        private void RunLoadGameWindowClick(object sender, RoutedEventArgs e)
        {
            LoadGameWindow loadGameWindow = new LoadGameWindow();
            loadGameWindow.Show();
            this.Close();
        }

        private void RunHighScoreWindowClick(object sender, RoutedEventArgs e)
        {
            HighScoreWindow loadHighScoresWindow = new HighScoreWindow();
            loadHighScoresWindow.Show();
            this.Close();
        }

        private void ExitClick(object sender, RoutedEventArgs e)
        {
            Environment.Exit(0);
        }
    }
}
