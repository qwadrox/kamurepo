﻿// <copyright file="ITankModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Urlenyinvazio.Model.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;

    public interface ITankModel
    {
        Rect Area { get; }

        new void ChangeX(double diff);
        new void ChangeY(double diff);
    }
}
