﻿// <copyright file="IGameModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Urlenyinvazio.Model.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Urlenyinvazio.Model.ModelClasses;
    public interface IGameModel
    {
        int HighScore { get; set; }

        int Time { get; set; }

        List<Alien> AlienList { get; set; }

        List<MyShape> Bullet { get; set; }

        ITankModel GetTank { get; set; }
    }
}
