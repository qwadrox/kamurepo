﻿// <copyright file="TankModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Urlenyinvazio.Model.ModelClasses
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using Urlenyinvazio.Model.Interfaces;
    using Urlenyinvazio.Model.ModelClasses;

    public class TankModel : MyShape, ITankModel
    {

        public Rect Area {

            get { return base.Area; }
        }

        /// <summary>
        /// Nincs kesz.
        /// </summary>
        /// <param name="diff">asd.</param>
        public new void ChangeX(double diff)
        {
            base.ChangeX(diff);
        }

        /// <summary>
        /// Nincs kesz.
        /// </summary>
        /// <param name="diff">asd.</param>
        public new void ChangeY(double diff)
        {
            base.ChangeY(diff);
        }

        public TankModel(double x, double y, double w, double h)
            : base(x, y, w, h)
        {
        }
        
    }
}
