﻿// <copyright file="GameModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Urlenyinvazio.Model
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Urlenyinvazio.Model.Interfaces;
    //using Urlenyinvazio.Model.Modelclasses;
    using Urlenyinvazio.Model.ModelClasses;

    /// <summary>
    /// Game model class.
    /// </summary>
    public class GameModel : IGameModel
    {

        /// <summary>
        /// Gets or sets property for HighScore.
        /// </summary>
        public int HighScore { get; set; }

        public int Time { get; set; }

        // <summary>
        /// Gets or sets property for AlienList.
        /// </summary>
        public List<Alien> AlienList { get; set; }

        public List<MyShape> Bullet { get; set; }

        public ITankModel GetTank { get; set; }

        public GameModel()
        {
            this.GetTank = new TankModel((Config.BgWidth / 2) - (Config.TankWidth / 2), Config.BgHeight - (Config.TankHeight + 2), Config.TankWidth, Config.TankHeight);
            this.AlienList = new List<Alien>();
            Bullet = new List<MyShape>();
        }
    }
}
