﻿// <copyright file="Alien.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Urlenyinvazio.Model.ModelClasses
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Urlenyinvazio.Model.Interfaces;

    /// <summary>
    /// Not ready.
    /// </summary>
    public class Alien : MyShape
    {
        /// <summary>
        /// Enum Not ready.
        /// </summary>
        public enum AlienType
        {
            /// <summary>
            /// Not ready.
            /// </summary>
            Alien1,

            /// <summary>
            /// Not ready.
            /// </summary>
            Alien2,

            /// <summary>
            /// Not ready.
            /// </summary>
            Alien3,

            /// <summary>
            /// Not ready.
            /// </summary>
            Alien4,
        }

        public Alien(double x, double y, double w, double h)
            : base(x, y, w, h)
        {
        }

        /// <summary>
        /// Gets or sets AlienDamage.
        /// </summary>
        public double AlienDamage { get; set; }

        /// <summary>
        /// Gets or sets AlienSpeed.
        /// </summary>
        public double AlienSpeed { get; set; }

        /// <summary>
        /// Gets or sets AlienHeatlh.
        /// </summary>
        public int AlienHeatlh { get; set; }

        public AlienType GetAlienType { get; set; }

        /// <summary>
        /// Overriding the ToString() method.
        /// </summary>
        /// <returns>The type of aliens in string.</returns>
        public override string ToString()
        {
            switch (this.GetAlienType)
            {
                case AlienType.Alien1:
                    return "alien1";
                case AlienType.Alien2:
                    return "alien2";
                case AlienType.Alien3:
                    return "alien3";
                case AlienType.Alien4:
                    return "alien4";
            }

            return "default";
        }

    }
}
