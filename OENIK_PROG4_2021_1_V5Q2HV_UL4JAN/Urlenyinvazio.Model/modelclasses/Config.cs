﻿// <copyright file="Config.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Urlenyinvazio.Model
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Config class for static fields.
    /// </summary>
    public static class Config
    {
        /// <summary>
        /// Width of the background.
        /// </summary>
        public const int BgWidth = 450;

        /// <summary>
        /// Height of background.
        /// </summary>
        public const int BgHeight = 675;

        /// <summary>
        /// Height of background.
        /// </summary>
        public const int AlienHight = 50;

        /// <summary>
        /// Height of background.
        /// </summary>
        public const int AlienWidth = 50;

        public const int TankWidth = 60;

        public const int TankHeight = 78;

        public const int BulletWidth = 5;

        public const int BulletHeight = 10;
    }
}
