﻿// <copyright file="GameController.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Urlenyinvazio.Controller
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Threading;
    using Urlenyinvazio.Logic.LogicClasses;
    using Urlenyinvazio.Model;
    using Urlenyinvazio.Model.Interfaces;
    using Urlenyinvazio.Renderer;

    public class GameController : FrameworkElement
    {
        private IGameModel model;
        private GameLogic logic;
        private GameRenderer renderer;
        DispatcherTimer timer;
        DispatcherTimer alienSpawnTimer;
        DispatcherTimer alienMoveTimer;

        public GameController()
        {
            this.Loaded += this.Controller_Loaded;
        }

        public GameController(IGameModel model)
        {
            this.model = model;
            this.Loaded += this.Controller_Loaded;
        }

        protected override void OnRender(DrawingContext drawingContext)
        {
            if (renderer != null) drawingContext.DrawDrawing(renderer.BuildDrawing());
        }

        private void Controller_Loaded(object sender, RoutedEventArgs e)
        {
            if (this.model == null)
            {
                this.model = new GameModel();
            }

            this.renderer = new GameRenderer(this.model);
            this.logic = new GameLogic(this.model);
            Window win = Window.GetWindow(this);

            if (win != null) // if (!IsInDesignMode)
            {

                timer = new DispatcherTimer();
                timer.Interval = TimeSpan.FromMilliseconds(25);
                timer.Tick += Timer_Tick1;
                timer.Start();

                alienSpawnTimer = new DispatcherTimer();
                alienSpawnTimer.Interval = TimeSpan.FromMilliseconds(3000);
                alienSpawnTimer.Tick += AlienTimer_Tick;
                alienSpawnTimer.Start();

                alienMoveTimer = new DispatcherTimer();
                alienMoveTimer.Interval = TimeSpan.FromMilliseconds(750);
                alienMoveTimer.Tick += AlienMoveTimer_Tick;
                alienMoveTimer.Start();
                win.KeyDown += this.Win_KeyDown; // += <TAB><RET>
            }

            logic.RefreshScreen += (obj, args) => this.InvalidateVisual();
        }

        private void OpenSaveAndExitWindow(IGameModel model)
        {
             SaveAndExitWindow saveAndExitWindow = new SaveAndExitWindow(model)
             {
                Visible = true,
             };
        }
        private void AlienMoveTimer_Tick(object sender, EventArgs e)
        {
            logic.MoveAlien();
        }

        private void AlienTimer_Tick(object sender, EventArgs e)
        {
            logic.addAlien();
        }

        private void Timer_Tick1(object sender, EventArgs e)
        {
            logic.MoveBullet();
        }
        private void Win_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Left: this.logic.MoveTank(GameLogic.Direction.Left); break;
                case Key.Right: this.logic.MoveTank(GameLogic.Direction.Right); break;
                case Key.Space: this.logic.Fire(); break;
                case Key.Escape: this.OpenSaveAndExitWindow(this.model); break;
            }

            this.InvalidateVisual();
        }
    }
}
