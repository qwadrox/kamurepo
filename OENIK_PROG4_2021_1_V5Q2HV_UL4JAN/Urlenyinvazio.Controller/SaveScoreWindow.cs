﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Urlenyinvazio.Logic.Interfaces;
using Urlenyinvazio.Logic.LogicClasses;
using Urlenyinvazio.Model.Interfaces;

namespace Urlenyinvazio.Controller
{
    public partial class SaveScoreWindow : Form
    {
        private readonly IGameModel model;
        private readonly IRepositoryLogic repositoryLogic;
        public SaveScoreWindow(IGameModel model)
        {
            InitializeComponent();

            this.model = model;
            this.repositoryLogic = new RepositoryLogic();
        }

        private void SaveButtonClick(object sender, EventArgs e)
        {
            this.repositoryLogic.SaveHighScore(this.model, this.textBox1.Text);
            MessageBox.Show("A Score elmentve.");
            this.Close();
            Environment.Exit(0);
        }
    }
}
