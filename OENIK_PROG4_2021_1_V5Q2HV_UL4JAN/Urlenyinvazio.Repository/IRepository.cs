﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Urlenyinvazio.Repository
{
    public interface IRepository<TGameModel, THighScore>
    {

        TGameModel LoadGame(string filename);


        void SaveGame(TGameModel game, string fileName);

        List<THighScore> LoadHighScore();


        void SaveHighScore(THighScore highscore);
    }
}
