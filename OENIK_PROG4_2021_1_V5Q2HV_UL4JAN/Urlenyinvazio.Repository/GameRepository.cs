﻿// <copyright file="Repository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Urlenyinvazio.Repository
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Newtonsoft.Json;
    using Urlenyinvazio.Model;
    using Urlenyinvazio.Model.Interfaces;
    using Urlenyinvazio.Model.ModelClasses;

    public class GameRepository : IRepository<IGameModel, SavedGame>
    {
        public IGameModel LoadGame(string fileName)
        {
            GameModel gameModel = new GameModel();
            using (StreamReader r = new StreamReader($"{fileName}.json"))
            {
                string json = r.ReadToEnd();
                gameModel = JsonConvert.DeserializeObject<GameModel>(json, new JsonSerializerSettings
                {
                    TypeNameHandling = TypeNameHandling.Objects,
                    MissingMemberHandling = MissingMemberHandling.Ignore,
                });
                return gameModel;
            }
        }

        public void SaveGame(IGameModel game, string fileName)
        {
            string jsonData = JsonConvert.SerializeObject(game, Formatting.Indented, new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.Objects,
                TypeNameAssemblyFormat = System.Runtime.Serialization.Formatters.FormatterAssemblyStyle.Simple,
            });
            jsonData = jsonData.Substring(0, jsonData.Length - 1) + $", \"SaveDate\": \"{DateTime.Now}\" }}";

            File.WriteAllText($"{fileName}.json", jsonData);
        }

        public List<SavedGame> LoadHighScore()
        {
            string filePath = System.IO.Path.GetFullPath("highScores.json");
            string fileName = "highScores.json";
            List<SavedGame> highScores = new List<SavedGame>();
            try
            {
                using (StreamReader sr = new StreamReader(filePath))
                {
                    string json = sr.ReadToEnd();
                    try
                    {
                        highScores = JsonConvert.DeserializeObject<List<SavedGame>>(json);
                        return highScores;
                    }
                    catch (Exception)
                    {
                        highScores.Add(JsonConvert.DeserializeObject<SavedGame>(json));
                        return highScores;
                    }
                }
            }
            catch (FileNotFoundException)
            {
                SavedGame nosavedgame = new SavedGame()
                {
                    Date = default,
                    Name = "Nincs megjeleníthető eredmény!",
                    Score = 0,
                    Time = 0,
                };

                highScores.Add(nosavedgame);
                return highScores;
            }
        }

        public void SaveHighScore(SavedGame highScore)
        {
            try
            {
                string jsonData = File.ReadAllText("highscores.json");
                List<SavedGame> highScoresList;
                if (jsonData.Contains('['))
                {
                    highScoresList = JsonConvert.DeserializeObject<List<SavedGame>>(jsonData)
                        ?? new List<SavedGame>();
                }
                else
                {
                    highScoresList = new List<SavedGame>
                    {
                        JsonConvert.DeserializeObject<SavedGame>(jsonData),
                    };
                }

                highScoresList.Add(highScore);
                jsonData = JsonConvert.SerializeObject(highScoresList, Formatting.Indented);
                File.WriteAllText("highscores.json", jsonData);
            }
            catch (FileNotFoundException)
            {
                // first highscore saved
                string jsonData = JsonConvert.SerializeObject(highScore, Formatting.Indented);
                using (StreamWriter file = new StreamWriter("highScores.json"))
                {
                }

                File.WriteAllText("highscores.json", jsonData);
            }
        }
    }
}
