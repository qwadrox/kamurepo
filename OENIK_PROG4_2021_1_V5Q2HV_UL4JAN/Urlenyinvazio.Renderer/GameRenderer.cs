﻿// <copyright file="GameRenderer.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Urlenyinvazio.Renderer
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using Urlenyinvazio.Model;
    using Urlenyinvazio.Model.Interfaces;
    using Urlenyinvazio.Model.ModelClasses;

    public class GameRenderer
    {
        private readonly IGameModel model;
        private Drawing background;
        private Drawing oldPlayer;
        private readonly Dictionary<string, Brush> myBrushes = new Dictionary<string, Brush>();
        private Point oldPlayerPosition = new Point(-1, -1);
        public GameRenderer(IGameModel model)
        {
            this.model = model;
        }

        public Drawing BuildDrawing()
        {
            DrawingGroup dg = new DrawingGroup();
            dg.Children.Add(this.GetBackground());
            dg.Children.Add(this.GetPlayer());


            foreach (MyShape star in this.model.Bullet)
            {
                Geometry g = new RectangleGeometry(star.Area);
                GeometryDrawing starGeo = new GeometryDrawing(Brushes.White, new Pen(Brushes.Red, 1), g);
                dg.Children.Add(starGeo);
            }

            foreach (var alien in model.AlienList)
            {
                Geometry g = new RectangleGeometry(alien.Area);
                GeometryDrawing alienGeo = new GeometryDrawing(Brushes.Red, new Pen(Brushes.Red, 1), g);
                dg.Children.Add(alienGeo);
            }

            return dg;
        }

        private Brush GetBrush(string fileName)
        {
            if (!this.myBrushes.ContainsKey(fileName))
            {
                BitmapImage bmp = new BitmapImage();
                bmp.BeginInit();
                bmp.StreamSource = Assembly.GetExecutingAssembly().GetManifestResourceStream(fileName);
                bmp.EndInit();
                ImageBrush ib = new ImageBrush(bmp);
                this.myBrushes[fileName] = ib;
            }

            return this.myBrushes[fileName];
        }

        /// <summary>
        /// Drawing the background.
        /// </summary>
        /// <returns>Return a Drawing.</returns>
        private Drawing GetBackground()
        {
            if (this.background == null)
            {
                Geometry backgroundGeometry = new RectangleGeometry(new Rect(0, 0, Config.BgWidth, Config.BgHeight));
                GeometryDrawing gd = new GeometryDrawing(this.GetBrush("Urlenyinvazio.Renderer.Assets.background.jpg"), null, backgroundGeometry);
                this.background = gd;
                return gd;
            }

            return this.background;
        }

        private Drawing GetPlayer()
        {
            Point modelTankCoordinates = new Point(this.model.GetTank.Area.X, this.model.GetTank.Area.Y);
            if (this.oldPlayer == null || this.oldPlayerPosition != modelTankCoordinates)
            {
                Geometry tank = new RectangleGeometry(new Rect(this.model.GetTank.Area.X, this.model.GetTank.Area.Y, Config.TankWidth, Config.TankHeight));
                this.oldPlayer = new GeometryDrawing(this.GetBrush("Urlenyinvazio.Renderer.Assets.tank.png"), null, tank);
                this.oldPlayerPosition = modelTankCoordinates;
            }

            return this.oldPlayer;
        }

        //private Drawing GetFire()
        //{
        //    GeometryDrawing starGeo = new GeometryDrawing();
        //    foreach (MyShape star in this.model.Bullet)
        //    {
        //        Geometry g = new RectangleGeometry(star.Area);
        //        starGeo = new GeometryDrawing(Brushes.White, new Pen(Brushes.Red, 1), g);
                
        //    }

        //    return starGeo;
        //}
    }
}
