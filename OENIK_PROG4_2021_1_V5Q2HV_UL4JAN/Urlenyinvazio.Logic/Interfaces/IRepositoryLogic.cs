﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Urlenyinvazio.Model.Interfaces;
using Urlenyinvazio.Model.ModelClasses;

namespace Urlenyinvazio.Logic.Interfaces
{
    public interface IRepositoryLogic
    {
        IGameModel LoadGameModel(string fileName);

        void SaveGameModel(IGameModel model, string text);


        void SaveHighScore(IGameModel model, string text);

        List<SavedGame> LoadHighScores();
    }
}
