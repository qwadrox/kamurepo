﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Urlenyinvazio.Logic.Interfaces;
using Urlenyinvazio.Model.Interfaces;
using Urlenyinvazio.Model.ModelClasses;
using Urlenyinvazio.Repository;

namespace Urlenyinvazio.Logic.LogicClasses
{
    public class RepositoryLogic : IRepositoryLogic
    {
        private readonly IRepository<IGameModel, SavedGame> repo;

        public RepositoryLogic()
        {
            this.repo = new GameRepository();
        }

        public RepositoryLogic(IRepository<IGameModel, SavedGame> repo)
        {
            this.repo = repo;
        }

        public IGameModel LoadGameModel(string fileName)
        {
            return this.repo.LoadGame(fileName);
        }

        public void SaveGameModel(IGameModel model, string text)
        {
            this.repo.SaveGame(model, text);
        }

        public void SaveHighScore(IGameModel model, string text)
        {
            SavedGame savedGame = new SavedGame { Name = text, Score = model.HighScore, Time = model.Time, Date = DateTime.Now };
            this.repo.SaveHighScore(savedGame);
        }
        public List<SavedGame> LoadHighScores()
        {
            return this.repo.LoadHighScore();
        }
    }
}
