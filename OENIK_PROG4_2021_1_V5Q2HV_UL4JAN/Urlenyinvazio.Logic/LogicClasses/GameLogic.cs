﻿// <copyright file="GameLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Urlenyinvazio.Logic.LogicClasses
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Urlenyinvazio.Logic.Interfaces;
    using Urlenyinvazio.Model;
    using Urlenyinvazio.Model.Interfaces;
    using Urlenyinvazio.Model.ModelClasses;

    public class GameLogic : IGameLogic
    {
        /// <summary>
        /// Game model.
        /// </summary>
        private readonly IGameModel model;

        Random r = new Random();

        public event EventHandler RefreshScreen;

        /// <summary>
        /// sdfasfl.
        /// </summary>
        public enum Direction
        {
            /// <summary>
            /// sdfd.
            /// </summary>
            Left,

            /// <summary>
            /// adsd.
            /// </summary>
            Right,
        }

        public GameLogic(IGameModel model)
        {
            this.model = model;
        }

        public void MoveTank(Direction d)
        {
            if (d == Direction.Left)
            {
                this.model.GetTank.ChangeX(-5);
            }
            else if (d == Direction.Right)
            {
                this.model.GetTank.ChangeX(5);
            }
        }

        public void Fire()
        {
            this.model.Bullet.Add(new Model.ModelClasses.MyShape(this.model.GetTank.Area.X + (this.model.GetTank.Area.Width / 2) - (Config.BulletWidth / 2), this.model.GetTank.Area.Y, Config.BulletWidth, Config.BulletHeight));
            this.MoveBullet();
        }

        public void MoveBullet()
        {
            foreach (MyShape item in model.Bullet)
            {
                item.ChangeY(-20);
            }

            this.RefreshScreen?.Invoke(this, EventArgs.Empty);
        }

        public void addAlien()
        {
            int rand;
            int typeRand;

            typeRand = this.r.Next(1, 4);

            rand = this.r.Next(0, Config.BgWidth - 50);

            Alien oneAlien = new Alien(rand, 0, 50, 50);

            oneAlien.GetAlienType = (Alien.AlienType)typeRand;

            oneAlien.AlienHeatlh = typeRand;

            this.model.AlienList.Add(oneAlien);

            this.RefreshScreen?.Invoke(this, EventArgs.Empty);

        }

        public void MoveAlien()
        {
            foreach (var alien in model.AlienList)
            {
                alien.ChangeY(+20);
            }

            this.RefreshScreen?.Invoke(this, EventArgs.Empty);
        }
    }
}
